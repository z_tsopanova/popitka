﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace popitka.Models
{
    public class ProjectContext: DbContext
    {
        public ProjectContext()
        {}

        public DbSet<Project> Projects { get; set; }
    }
}