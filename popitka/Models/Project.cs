﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace popitka.Models
{
    public class Project
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string category { get; set; }
        public string subcategory { get; set; }
        public int target { get; set; }
        public string result { get; set; }
    }
}