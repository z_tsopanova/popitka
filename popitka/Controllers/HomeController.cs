﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using popitka.Models;

namespace popitka.Controllers
{
    public class HomeController : Controller
    {
        ProjectContext db = new ProjectContext();
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(db.Projects.ToList().ToPagedList(pageNumber, pageSize));
        }

        [HttpGet]
        public ActionResult EditBook(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Project proj = db.Projects.Find(id);
            if (proj != null)
            {
                return View(proj);
            }
            return HttpNotFound();
        }



        [HttpPost]
        public ActionResult EditBook(Project proj)
        {
            db.Entry(proj).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Project proj)
        {
            db.Entry(proj).State = EntityState.Added;
            db.SaveChanges();
            return RedirectToAction("Index");
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            Project proj = db.Projects.Find(id);
            if (proj == null)
            {
                return HttpNotFound();
            }
            return View(proj);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Project proj = db.Projects.Find(id);

            if (proj == null)
            {
                return HttpNotFound();
            }
            db.Projects.Remove(proj);
            db.SaveChanges();
            return RedirectToAction("Index");

        }
    }
}